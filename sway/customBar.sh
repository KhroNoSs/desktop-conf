date=$(date +%x)
heure=$(date +%H:%M)
temps=$(sensors)
tempCPU=$(sensors k10temp-pci-00c3 | head -n 3 | tail -n 1 | grep -oE  "[0-9][0-9].[0-9]°C")
tempCG=$(sensors amdgpu-pci-0800 | head -n5 | tail -n 1 | grep -oE  "[0-9][0-9].[0-9]°C" | head -n 1)
tempNVME=$(sensors k10temp-pci-00c3 | head -n 3 | tail -n 1 | grep -oE  "[0-9][0-9].[0-9]°C")

echo "$heure | $date | 🌡️ $tempCPU / $tempCG/ $tempNVME //"
