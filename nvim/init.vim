set number
set tabstop=4
set shiftwidth=4
set clipboard+=unnamedplus
nnoremap <CR> :noh<CR><CR>
syn spell toplevel
set spelllang=fr
noremap <F2> :set spell!<cr>:set spell?<cr> "

" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

Plug 'neovim/nvim-lspconfig'
Plug 'rhysd/vim-grammarous'
Plug 'tpope/vim-sensible'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }
Plug 'godlygeek/tabular'
Plug 'pearofducks/ansible-vim'
" List ends here. Plugins become visible to Vim after this call.
call plug#end()

lua require('lspconfig').pyright.setup{}
" Installer shellcheck (repo).
lua require('lspconfig').bashls.setup{}
lua require('lspconfig').yamlls.setup{}
lua require('lspconfig').dockerls.setup{}
lua require('lspconfig').terraformls.setup{}
lua <<EOF
  require'lspconfig'.terraformls.setup{}
EOF
autocmd BufWritePre *.tfvars lua vim.lsp.buf.formatting_sync()
autocmd BufWritePre *.tf lua vim.lsp.buf.formatting_sync()
" Installer ansible-lint (repo) et ansible-vim (Vim Plug).
lua require('lspconfig').ansiblels.setup{}
